# los widgets son los componentes graficos
# Tk - contenedor base o raiz que forman la interface
# Frame - widget
# Label - etiqueta para mostrar texto estatico
# Entry - campo corto
# Text - texto largo
# Button - boton para hacer click
# radiobutton -  se usa para marcar una opcion
# checkbutton - boton cuadrado pra marcar con un tiic
# menu - estructura de botones
# boxes - ventanas emergentes que muestran info
# dialogs - ventanas emergentes que muestran info

# from tkinter import *
#
# def hola():
#     print("Hola mundo")
#
# def crearlabel():
#     Label(root, text="Label creada dinamicamente").pack()
#
# root = Tk()
#
# Button(root, text="Click me", command=crearlabel).pack()
#
#
# root.mainloop()

# from tkinter import *
#
# def seleccionar():
#     monitor.config(text="{}".format(opcion.get()))
#
# root = Tk()
#
# opcion = IntVar()
#
# Radiobutton(root, text="Opcion 1", value=1, command=seleccionar).pack()
# Radiobutton(root, text="Opcion 2", value=2, command=seleccionar).pack()
# Radiobutton(root, text="Opcion 3", value=3, command=seleccionar).pack()
#
# monitor = Label(root)
# monitor.pack()
#
# root.mainloop()
#
from tkinter import *

def seleccionar():
    monitor.config(text="{}".format(opcion.get()))

def reset():
    opcion.set(None)
    monitor.config(text="")



root = Tk()

opcion = IntVar()



Radiobutton(root, text="Opcion 1", variable=opcion, value=1, command=seleccionar).pack()
Radiobutton(root, text="Opcion 2", variable=opcion, value=2, command=seleccionar).pack()
Radiobutton(root, text="Opcion 3", variable=opcion, value=3, command=seleccionar).pack()

monitor = Label(root)
monitor.pack()

Button(root, text="Reiniciar", command=reset).pack()

root.mainloop()
































