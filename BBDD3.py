# SQLITE3 input of data

import sqlite3
import time
import datetime
import random

conn = sqlite3.connect('tutorial.db')
c = conn.cursor()

def create_table():
    c.execute('CREATE TABLE IF NOT EXISTS visitas (datestamp TEXT, keyword TEXT, value REAL)')

# def data_entry():
#     c.execute("INSERT INTO visitas('2016-01-02', 'python', 8)")
#     conn.commit()
#     c.close()
#     conn.close()

def dynamic_data_entry():
    # unix = time.time()
    datestamp = str(datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d  %H:%M:%S'))
    keyword = input("Insert name of the visita: ")
    value = random.randrange(0,10)
    c.execute("INSERT INTO visitas (datestamp, keyword, value) VALUES (?, ?, ?)",
              (datestamp, keyword, value))
    conn.commit()


create_table()
# data_entry()
for i in range(10):
    dynamic_data_entry()
    time.sleep(0.5)
c.close()
conn.close()



