# check io exercise
# test1 = [1,25,3]
# test2 = []
# for i in test1:
#     test2.append(str(i))
# print(test2)
# test = [(4, '4'), (3, '2'), (2, '6')]

from collections import Counter
from itertools import chain, repeat




items2= [4,6,2,2,2,6,4,4,4]
items = [17, 99, 42]
items1 = [4, 6, 2, 2, 6, 4, 4, 4]
items3 = ['bob', 'bob', 'carl', 'alex', 'bob']


def frequency_sort(items):

    return chain.from_iterable(repeat(*x) for x in Counter(items).most_common())

frequency_sort(items)