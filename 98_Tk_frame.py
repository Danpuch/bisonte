# los widgets son los componentes graficos
# Tk - contenedor base o raiz que forman la interface
# Frame - widget
# Label - etiqueta para mostrar texto estatico
# Entry - campo corto
# Text - texto largo
# Button - boton para hacer click
# radiobutton -  se usa para marcar una opcion
# checkbutton - boton cuadrado pra marcar con un tiic
# menu - estructura de botones
# boxes - ventanas emergentes que muestran info
# dialogs - ventanas emergentes que muestran info

from tkinter import *

root = Tk()
root.title("Hola Mundo")
root.resizable(1,1)   # el 1 es True, y el 0 es False (en ancho y en alto)
root.iconbitmap()   # aqui le podemos pasar una imagen *.ico ('ejemplo.ico)

root.config(width = 480, height = 320, cursor = "pirate")
# frame = Frame(root, width = 480, height = 320)  # se podria haber hecho así,
root.config(bg="blue")  # color de fondo
root.config(bd=15, relief="ridge")   # grosor en pixeles y el tipo de marco



frame = Frame(root)  # el frame esta dentro de la raiz
frame.pack(side="bottom", anchor="e")  # empaquetamos el frame para distribuirlo en la raiz
frame.pack(fill="x", expand=1)  # para que rellene horizontalmente (x) o vertical (y) o (both)
frame.config(width = 480, height = 320, cursor = "arrow")
# frame = Frame(root, width = 480, height = 320)  # se podria haber hecho así,
frame.config(bg="lightblue")  # color de fondo
frame.config(bd=20, relief="sunken")   # grosor en pixeles y el tipo de marco


# abajo del todos
root.mainloop()
