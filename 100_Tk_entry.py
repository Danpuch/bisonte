# los widgets son los componentes graficos
# Tk - contenedor base o raiz que forman la interface
# Frame - widget
# Label - etiqueta para mostrar texto estatico
# Entry - campo corto
# Text - texto largo
# Button - boton para hacer click
# radiobutton -  se usa para marcar una opcion
# checkbutton - boton cuadrado pra marcar con un tiic
# menu - estructura de botones
# boxes - ventanas emergentes que muestran info
# dialogs - ventanas emergentes que muestran info

from tkinter import *

root = Tk()

label = Label(root, text="Nombre muy largo").grid(row=0, column=0, sticky="e", padx=5, pady=5)
entry = Entry(root)
entry.grid(row=0, column=1, padx=5, pady=5)
entry.config(justify="right", state="disabled")  # o normal

label2 = Label(root, text="Contraseña").grid(row=1, column=0, sticky='e', padx=5, pady=5)
entry2 = Entry(root)
entry2.grid(row=1, column=1, padx=5, pady=5)
entry2.config(justify="center", show="*")


# frame1 = Frame(root)
# frame1.pack()
# entry = Entry(frame1)
# entry.pack(side="right")
# label = Label(frame1, text="Nombre  ")
# label.pack(side="left")
#
# frame2 = Frame(root)
# frame2.pack()
# entry2 = Entry(frame2)
# entry2.pack(side="right")
# label2 = Label(frame2, text="Apellidos")
# label2.pack(side="left")

# disposicion automatica en cuadrícula con el 'grid'








root.mainloop()