# vamos a realizar una base de datos de clientes

import sqlite3
import random

conn = sqlite3.connect('nuevabd2.db')
c = conn.cursor()


def crear_db():
    c.execute("CREATE TABLE IF NOT EXISTS clientes(id INTEGER PRIMARY KEY AUTOINCREMENT, name VARCHAR (100) NOT NULL , "
              "direccion VARCHAR(100) NOT NULL , canal VARCHAR(100) NOT NULL , fecha REAL)")

def data_entry():
    name = input("Nombre del cliente: ")
    direccion = input("Direccion: ")
    canal = input("Canal: ")
    fecha = random.randrange(0, 10)
    c.execute("INSERT INTO clientes(name, direccion, canal, fecha) VALUES (?, ?, ?, ?)",
              (name, direccion, canal, fecha))
    conn.commit()


def delete_db():
    c.execute("DELETE FROM clientes WHERE canal = 'asdf' ")

def drop_db():
    c.execute("DROP TABLE clientes")


def update_db():
    pass


# BLOQUE PRINCIPAL


# crear_db()
# for i in range(10):
#     data_entry()
delete_db()

c.close()
conn.close()
