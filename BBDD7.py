# ejercicios
import sqlite3
import time
import datetime

sueldo_base = 0
bono = 0.01
bono_fam = 0.05

base_imponible = sueldo_base + bono + bono_fam

FONASA = 0.07
AFS1 = 0.12
AFS2 = 0.114

conn = sqlite3.connect("trabajadores.db")
c = conn.cursor()

def create_database():

    c.execute('''CREATE TABLE IF NOT EXISTS trabajadores (
        id INTEGER PRIMARY KEY AUTOINCREMENT, name VARCHAR (100) NOT NULL , surname VARCHAR (100) NOT NULL , salary FLOAT NOT NULL , 
        afap INTEGER NOT NULL , entry REAL, hijos INTEGER)
    ''')

def entry_data():

    name = input("Introduce tu nombre: ")
    surname = input("Apellido: ")
    salary = float(input("Salario bruto "))
    afap = int(input("Afap [1 or 2] : "))
    entry = str(datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d  %H:%M:%S'))
    hijos = int(input("Cantidad de hijos: "))
    c.execute("INSERT INTO trabajadores(name, surname, salary, afap, entry, hijos) VALUES (?, ?, ?, ?, ?, ?)", (name, surname, salary, afap, entry, hijos))
    conn.commit()

def read_from_db():
    c.execute('SELECT * FROM trabajadores')
    data = c.fetchall()
    return data


def mostrar_datos(data):
    fonasa_temp = 0
    sueldos_afs1 = 0
    sueldos_afs2 = 0
    for i in data:
        print("*********************************")
        print("La base imponible de", i[1], "es de", i[3]+(i[3]*bono)+(i[3])*bono_fam*i[6])
        imponible_temp = i[3]+(i[3]*bono)+(i[3])*bono_fam*i[6]
        fonasa_temp += i[3]
        if i[4] == 1:
            print("Con el descuento de FONASA y AFS: ", imponible_temp - (imponible_temp * FONASA) - (imponible_temp * AFS1))
            sueldos_afs1 += i[3]
        elif i[4] == 2:
            print("Con el descuento de FONASA y AFS: ", imponible_temp - (imponible_temp * FONASA) - (imponible_temp * AFS2))
            sueldos_afs2 += i[3]
        else:
            print("No has introducido el AFS correctamente. ")

    print("*********************")
    print("*********************")
    print("*********************")
    print("El pago total a FONASA es : {}".format(fonasa_temp * FONASA))
    print("El pago a AFS1 es {}".format(sueldos_afs1 * AFS1))
    print("El pago a AFS2 es {}".format(sueldos_afs2 * AFS2))
    print("El promedio de los sueldos es: {} ".format(fonasa_temp / 10))


# bloque principal
# create_database()
# conta = 1
# for i in range(10):
#     print("****************")
#     print("Trabajador {}".format(conta))
#     entry_data()
#     conta += 1

data = read_from_db()

mostrar_datos(data)

c.close()
conn.close()
