# # ejercicios con python
# 1) Este programa pide primeramente la cantidad total de compras
# de una persona. Si la cantidad es inferior a $100.00, el programa
# dirá que el cliente no aplica a la promoción. Pero si la persona
# ingresa una cantidad en compras igual o superior a $100.00, el
# programa genera de forma aleatoria un número entero del cero al cinco.
#     Cada número corresponderá a un color diferente de cinco colores de
# bolas que hay para determinar el descuento que el cliente recibirá como
# premio. Si la bola aleatoria es color blanco, no hay descuento, pero
# si es uno de los otros cuatro colores, sí se aplicará un descuento
# determinado según la tabla que  aparecerá, y ese descuento se aplicará
# sobre el total de compra que introdujo inicialmente el usuario, de
# manera que el programa mostrará un nuevo valor a pagar luego de haber
# aplicado el descuento.

# import random
#
# print("Bienvenido a la tienda PUCH")
# compras = float(input("Introduce el total de tu compra: "))
#
# if compras < 100:
#     print("No aplican promociones por menos de 100 € en compras")
# else:
#     print("Felcidades, tienes promoción: ")
#     print('''
#            COLOR              DESCUENTO
#         bola blanca         No tiene descuento
#         bola roja           10 % de descuento
#         bola azúl           20 % de descuento
#         bola verde          25 % de descuento
#         bola amarilla       50 % de descuento
#     ''')
#
#     color = random.randint(0,4)
#     if color == 0:
#         print("Te ha tocado la bola blanca. Sin descuento.")
#     elif color == 1:
#         print("Te ha tocado la bola roja")
#         compras = compras - (compras * 0.10)
#     elif color == 2:
#         print("Te ha tocado la bola azul")
#         compras = compras - (compras * 0.20)
#     elif color == 3:
#         print("Te ha tocado la bola verde")
#         compras = ccompras - (compras * 0.25)
#     elif color == 4:
#         print("Te ha tocado la bola amarilla")
#         compras = compras - (compras * 0.50)
#     else:
#         print("Algo está mal en el programa!!!!")
#
#     print("Su nuevo total a pagar es: {}".format(compras))
#
#     print(color)


# Este programa muestra primero el listado de categorías de películas
# y pide al usuario que introduzca el código de la categoría de la
# película y posterior a ello pide que el usuario introduzca el número
# de días de atraso, y así se muestra al final el total a pagar.

#
# salir = 'y'
# while salir == 'y':
#     print("Bienvenido al videoclub del barrio ")
#     print('''
#
#             CATEGORIA        PRECIO      CODIGO        DIAS
#
#             FAVORITOS       $ 2.50          1           $ 0.50
#             NUEVOS          $ 3.00          2           $ 0.75
#             ESTRENOS        $ 3.50          3           $ 1.00
#             SUPER ESTRENOS  $ 4.00          4           $ 1.50
#
#     ''')
#
#     seleccion = int(input("IINTRODUZCA EL CODIGO DE LA CATEGORIA DE LA PELICULA: "))
#     dias = int(input("INTRODUZCA LOS DIAS DE ALQUILER: "))
#
#     variable = 0
#
#     if seleccion == 1:
#         variable = 2.50 + (0.50*dias)
#
#     elif seleccion == 2:
#         variable = 3.00 + (0.75*dias)
#
#     elif seleccion == 3:
#         variable = 3.50 + (1.00*dias)
#
#     elif seleccion == 4:
#         variable = 4.00 + (1.50*dias)
#
#     else:
#         print("No has seleccionado ninguna categoria valida.")
#
#     print("El total a pagar es: {}".format(variable))
#     salir = int(input("¿Quiere devolver otra pelicula? [y/n] : "))
#
# print("Gracias por usar nuestro servicio de videoclub. ")


#
# 2) De la galería de productos, el usuario introducirá el código y el número
# de unidades del producto que desea comprar. El programa determinará el total
# a pagar, como una factura.
#     Una variante a este ejercicio que lo haría un poco más complejo sería dar
# la posibilidad de seguir ingresando diferentes códigos de productos con sus
# respectivas cantidades, y cuando el usuario desee terminar el cálculo de la factura
# completa con todas sus compras. Te animas??
#
# import os
#
# total_def = 0
# salir = 'y'
# while salir == 'y':
#
#     print('''
#
#     ********* BIENVENIDO A ZARA ***********
#
#             ELIJA EL PRODUCTO DESEADO:
#
#         CODIGO      PRODUCTO       PRECIO
#
#         1 ..........CAMISA .........25 €
#         2...........CINTURON .......15 €
#         3...........ZAPATOS ........45 €
#         4...........PANTALON .......35 €
#         5...........CALCETINES ......9 €
#         6...........FALDAS .........24 €
#         7...........GORRAS .........12 €
#         8...........CORBATA .........9 €
#         9...........CAMISETA .......19 €
#     ''')
#
#     articulo = int(input("Introduzca el codigo del articulo: "))
#     unidades = int(input("¿Cuantas unidades quieres?: "))
#     precio = 0
#
#     if articulo == 1:
#         precio = 25
#     elif articulo == 2:
#         precio = 15
#     elif articulo == 3:
#         precio = 45
#     elif articulo == 4:
#         precio = 35
#     elif articulo == 5:
#         precio = 9
#     elif articulo == 6:
#         precio = 24
#     elif articulo == 7:
#         precio = 12
#     elif articulo == 8:
#         precio = 9
#     elif articulo == 9:
#         precio = 19
#     else:
#         print("No has introducido ningun codigo valido. Saliendo del programa .... ")
#
#     total = precio * unidades
#     total_def += total
#
#     print("El precio es de {} € ".format(total))
#     print("Llevas acumulado {} € en el carrito".format(total_def))
#     salir = input("Quieres comprar otro producto? [y/n]:  ")
#     os.system("clear")
#
# print("Hasta pronto!!")

# Una PyME, tiene la siguiente estructura de pagos para sus 10 empleados:
# Un sueldo base
# Una bonificación del 1% del sueldo base, por cada mes trabajado
# Una asignación familiar del 5% del sueldo base, por cada hijo
#
# La suma de los tres valores anteriores, conforman la “base imponible”.
# Todos los empleados están en FONASA, así que deben cotizar el 7% de la
# base imponible en salud. Los empleados están en una de dos:
# AFPs, la primera cobra (entre imposición y otros gastos) el 12 % de la
# base imponible, mientras que la segunda cobra el 11.4%
#
# Construyan un programa Python que:
#
# a) Pida el ingreso de datos de los 10 empleados y los almacene. Debe pedir:
# nombre, apellido, sueldo base, afap, fecha de ingreso y cantidad de hijos.
#
#     b) El programa debe calcular la base imponible, según lo indicado arriba
# y luego descontar según corresponda.
#
#     c) El programa debe calcular lo que se debe pagar a FONASA y el
# monto de cada AFAP.
#
#     d) El programa debe calcular los promedios de pago a los empleados
#
# e) El programa debe implementar control de excepciones en cada ingreso de
# información.
#
#     El mensaje debe ser claro al usuario, indicando que debe corregir en
# cada intento de ingresar los datos.E
#
# Se entiende por:
#
# FONASA: El Fondo Nacional de Salud, FONASA, es el organismo público
# encargado de otorgar cobertura de atención, tanto a las personas que
# cotizan el 7% de sus ingresos mensuales en FONASA, como a aquellas que,
# por carecer de recursos propios, financia el Estado a través de un
# aporte fiscal directo.
#
# AFAP: AFAP significa “Administradora de Fondos de Ahorro Previsional”.
# Son empresas que administran parte del aporte de los trabajadores afiliados
# para que en el futuro tengan una mejor jubilación.


















