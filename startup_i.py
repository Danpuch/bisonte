# cre un script llamado contador.py que realicec las siguientes tareas sobre un fichero llamado contador.txt
# que almacena un contador de visitas.

from io import open
import pickle

seleccion = input("Quieres sumar o restar visitas?: ")


fichero = open("contador.txt", "a+")
fichero.seek(0)
contenido = fichero.readline()
fichero.close()

if len(contenido) == 0:
    contenido = '0'
    fichero.write(contenido)
    print("Se ha creado el archivo contador!")

try:
    if seleccion.startswith('s'):
        contenido = int(contenido)
        temp = contenido + 1
        temp1 = str(temp)
        fichero.seek(0)
        fichero.write(temp1)
        fichero.close()
        print(temp1)
    elif seleccion.startswith('r'):
        contenido = int(contenido)
        temp = contenido - 1
        temp1 = str(temp)
        fichero.seek(0)
        fichero.write(temp1)
        fichero.close()
        print(temp1)
    else:
        print("Hay error")
finally:
    print("Hay error 22")

