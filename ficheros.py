# creamos un fichero de texto en persistencia

texto = '''

from:PeterSeller@rockroll.com
to:guardian@hotstamp.com

Hello Nicola, 
I'm Jeffrey from 'The Peter Seller's' Rock band. I contact you in order to 
inform that we cannot attend the concert next November 27th. 

Best regards, 

'''

# file = open("textfile.txt", 'w')
# file.write("Este texto se ha incorporado desde Python")
# file.close()
#
file = open("textfile.txt", 'r')
message = file.readlines()
for i in message:
    print(i)
# print(message)
file.close()

# file = open("textfile.txt", 'a')
# file.write(texto)
# file.close()
