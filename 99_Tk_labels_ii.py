# check io exercise
# test1 = [1,25,3]
# test2 = []
# for i in test1:
#     test2.append(str(i))
# print(test2)
# test = [(4, '4'), (3, '2'), (2, '6')]

from collections import OrderedDict

items = [4,6,2,2,2,6,4,4,4]
items1 = [17, 99, 42]
items3 = [4, 6, 2, 2, 6, 4, 4, 4]
items2 = ['bob', 'bob', 'carl', 'alex', 'bob']

def frequency_sort (items):

# pasamos todoo a string
    nlistastr = []

    for i in items:
        nlistastr.append(str(i))

# cramos 2 listas simultaneas
    d = OrderedDict()
    li_numero = []

    for i in items:
        if i not in li_numero:
            li_numero.append(i)
            d[i] = 1
        elif i in li_numero:
            d[i] += 1

    test = []
    for i in d:
        test.append(d[i])

    nl = []
    for i in range(len(test)):
        nl.append((test[i], li_numero[i]))

# ordenamos
    nl.sort(key=lambda primero:primero[0], reverse=True)

    nl1 = []
    for i in range(0, len(nl)):
        for k in range(nl[i][0]):
            nl1.append(nl[i][1])

    for i in d:
        for k in range(1, d[i]+1):
            nl.append(str(i))

    return nl1



if __name__ == '__main__':
    print("Example:")
    print(frequency_sort(items))

    # These "asserts" are used for self-checking and not for an auto-testing
    # assert list(frequency_sort([4, 6, 2, 2, 6, 4, 4, 4])) == [4, 4, 4, 4, 6, 6, 2, 2]
    # assert list(frequency_sort(['bob', 'bob', 'carl', 'alex', 'bob'])) == ['bob', 'bob', 'bob', 'carl', 'alex']
    # assert list(frequency_sort([17, 99, 42])) == [17, 99, 42]
    # assert list(frequency_sort([])) == []
    # assert list(frequency_sort([1])) == [1]
    print("Coding complete? Click 'Check' to earn cool rewards!")

