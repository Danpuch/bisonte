# los widgets son los componentes graficos
# Tk - contenedor base o raiz que forman la interface
# Frame - widget
# Label - etiqueta para mostrar texto estatico
# Entry - campo corto
# Text - texto largo
# Button - boton para hacer click
# radiobutton -  se usa para marcar una opcion
# checkbutton - boton cuadrado pra marcar con un tiic
# menu - estructura de botones
# boxes - ventanas emergentes que muestran info
# dialogs - ventanas emergentes que muestran info

from tkinter import *

def seleccionar():
    cadena = ''
    if (leche.get()):
        cadena += "con leche"
    else:
        cadena += "sin leche"

    if (azucar.get()):
        cadena += " y con azucar"
    else:
        cadena += " y sin azucar"

    monitor.config(text=cadena)


root = Tk()
root.title("Cafeteria")
root.config(bd=15)

leche = IntVar()   # 1 seria que sí, 0 es No
azucar = IntVar()

imagen = PhotoImage(file="imagen.gif")
Label(root, image=imagen).pack(side="left")

frame=Frame(root)
frame.pack(side="left")

Label(root, text="¿Como quieres el café?").pack(anchor="w" )
Checkbutton(root, text="Con leche", variable=leche, onvalue=1, offvalue=0, command=seleccionar).pack(anchor="w" )
Checkbutton(root, text="Con azucar", variable=azucar, onvalue=1, offvalue=0, command=seleccionar).pack(anchor="w" )

monitor = Label(frame)
monitor.pack()

root.mainloop()















