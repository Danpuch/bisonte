import re


# Crear un módulo para validación de nombres de usuarios. Dicho módulo, deberá cumplir con los siguientes criterios de aceptación:
#
# El nombre de usuario debe contener un mínimo de 6 caracteres y un máximo de 12.
# El nombre de usuario debe ser alfanumérico.
# Nombre de usuario con menos de 6 caracteres, retorna el mensaje "El nombre de usuario debe contener al menos 6 caracteres".
# Nombre de usuario con más de 12 caracteres, retorna el mensaje "El nombre de usuario no puede contener más de 12 caracteres".
# Nombre de usuario con caracteres distintos a los alfanuméricos, retorna el mensaje "El nombre de usuario puede contener solo letras y números".
# Nombre de usuario válido, retorna True.

def user():
    name = input("Nombre de usuario: ")

    if len(name) <6:
        print("El nombre de usuario debe contener al menos 6 caracteres")
    elif len(name) > 12:
        print("El nombre de usuario no puede contener más de 12 caracteres")
    elif name.isalnum() == False :
        print("El nombre de usuario puede conener solo letras y numeros")
    else:
        print("Usuario registrado correctamente!!!")


# Crear un módulo para validación de contraseñas. Dicho módulo, deberá cumplir con los siguientes criterios de aceptación:
#
# La contraseña debe contener un mínimo de 8 caracteres.
# Una contraseña debe contener letras minúsculas, mayúsculas, números y al menos 1 carácter no alfanumérico.
# La contraseña no puede contener espacios en blanco.
# Contraseña válida, retorna True.a
# Contraseña no válida, retorna el mensaje "La contraseña elegida no es segura".


def pasword():
    passw = input("Introduzca una password: ")

    if len(passw) < 8:
        print("La password debe tener un míninmo de 8 caracteres")
    elif ' ' in passw:
        print("La password no puede contener espacios")

    # expresiones regulares:

    if re.search("[a-z]", passw):
        print("OK")
        if re.search("[A-Z]", passw) :
            print("OK")
            if re.search("[0-9]", passw):
                print("OK")
                if re.search("[(+?¿!-)]", passw):
                    print("La pass es MUY segura. Felicidades")

    else:
        print("La pass no es segura")

#
# 6.6.3. Ejercicio 3
#
# Crear un módulo que solicite al usuario el ingreso de un nombre de usuario y contraseña y que los
# valide utilizando los módulos generados en los dos ejercicios anteriores.
#
# Ayuda: para contar la cantidad de caracteres de una cadenaa,
# en Python se utiliza la función incorporada: len(cadena).
#

user()
pasword()
