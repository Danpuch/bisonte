# SQLITE3 input of data

import sqlite3
import time
import datetime
import random

conn = sqlite3.connect('mydatabase.db')
c = conn.cursor()

def create_table():
    c.execute("CREATE TABLE IF NOT EXISTS usuarios(unix REAL, datestamp TEXT, keyword TEXT, value REAL)")

def data_entry():
    c.execute("INSERT INTO usuarios VALUES(123)")
    conn.commit()
    c.close()
    conn.close()


def dynamic_data_entry():
    unix = time.time()
    date = str(datetime.datetime.fromtimestamp(unix).strftime('%Y-%m-%d  %H:%M:%S'))
    keyword = input("What is your favourite animal?: ")
    value = random.randrange(0,10)
    c.execute("INSERT INTO usuarios (unix, datestamp, keyword, value) VALUES (?,?,?,?)",
              (unix, date, keyword, value))
    conn.commit()

create_table ()

for i in range(10):
    dynamic_data_entry()
    time.sleep(0.5)
c.close()
conn.close()







