# los widgets son los componentes graficos
# Tk - contenedor base o raiz que forman la interface
# Frame - widget
# Label - etiqueta para mostrar texto estatico
# Entry - campo corto
# Text - texto largo
# Button - boton para hacer click
# radiobutton -  se usa para marcar una opcion
# checkbutton - boton cuadrado pra marcar con un tiic
# menu - estructura de botones
# boxes - ventanas emergentes que muestran info
# dialogs - ventanas emergentes que muestran info

# from tkinter import *
#
# def hola():
#     print("Hola mundo")
#
# def crearlabel():
#     Label(root, text="Label creada dinamicamente").pack()
#
# root = Tk()
#
# Button(root, text="Click me", command=crearlabel).pack()
#
#
# root.mainloop()

from tkinter import *

def sumar():
    r.set(float(n1.get()) + float(n2.get()))
    borrar()

def restar():
    r.set(float(n1.get()) - float(n2.get()))
    borrar()


def producto():
    r.set(float(n1.get()) * float(n2.get()))
    borrar()



def borrar():
    n1.set('')
    n2.set('')


root = Tk()

n1 = StringVar()
n2 = StringVar()
r = StringVar()

Label(root, text="Numero 1: ").pack()
Entry(root, justify="center", textvariable=n1).pack() # primer numero
Label(root, text="Numero 2: ").pack()
Entry(root, justify="center", textvariable=n2).pack() # segundo numero
Button(root, text="Sumar", command=sumar).pack(side="left")
Button(root, text="Restar", command=restar).pack()
Button(root, text="Producto", command=producto).pack()

Label(root, text="\nResultado").pack()
Entry(root, justify="center", textvariable=r, state="disabled").pack() # resultado



# finalmente bucle de la aplicacion
root.mainloop()




