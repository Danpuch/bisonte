# SQLITE3 input of data

import sqlite3
import time
import datetime
import random
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from matplotlib import style
style.use("fivethirtyeight")



conn = sqlite3.connect('tutorial.db')
c = conn.cursor()

def create_table():
    c.execute('CREATE TABLE IF NOT EXISTS visitas (unix REAL, datestamp TEXT, keyword TEXT, value REAL)')

def data_entry():
    c.execute("INSERT INTO visitas(12123123, '2016-01-02', 'python', 8)")
    conn.commit()
    c.close()
    conn.close()

def dynamic_data_entry():
    unix = time.time()
    datestamp = str(datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d  %H:%M:%S'))
    keyword = input("Insert name of the visita: ")
    value = random.randrange(0,10)
    c.execute("INSERT INTO visitas (unix, datestamp, keyword, value) VALUES (?, ?, ?, ?)",
              (unix, datestamp, keyword, value))
    conn.commit()

def read_from_db():
    c.execute('SELECT * FROM visitas WHERE value=0 OR value=3')
    # data = c.fetchall()
    # print(data)
    for i in c.fetchall():
        print(i[1], i[2])

def graph_data():
    c.execute('SELECT unix, value FROM visitas')
    dates = []
    values = []
    for row in c.fetchall():
        #print(row[0])
        #print(datetime.datetime.fromtimestamp(row[0]))

        dates.append(datetime.datetime.fromtimestamp(row[0]))
        values.append(row[1])
    plt.plot_date(dates, values,'-')
    plt.show()


#create_table()
#data_entry()
# for i in range(10):
#     dynamic_data_entry()
#     time.sleep(0.5)
# read_from_db()
graph_data()
c.close()
conn.close()



