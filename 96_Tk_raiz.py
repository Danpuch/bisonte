# los widgets son los componentes graficos
# Tk - contenedor base o raiz que forman la interface
# Frame - widget
# Label - etiqueta para mostrar texto estatico
# Entry - campo corto
# Text - texto largo
# Button - boton para hacer click
# radiobutton -  se usa para marcar una opcion
# checkbutton - boton cuadrado pra marcar con un tiic
# menu - estructura de botones
# boxes - ventanas emergentes que muestran info
# dialogs - ventanas emergentes que muestran info

from tkinter import *

root = Tk()
root.title("Hola Mundo")
root.resizable(0,0)   # el 1 es True, y el 0 es False (en ancho y en alto)
root.iconbitmap()   # aqui le podemos pasar una imagen *.ico ('ejemplo.ico)

# un truco:
# cambiando la extensio de py a pyw , no se abre la terminal por detras de la ventana

# abajo del todos
root.mainloop()
